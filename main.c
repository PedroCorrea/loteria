#include <stdio.h>
#include <stdlib.h>

void preenche(int a[5][9]){
    int i, j;
    for(i = 0; i < 5; i++){
        printf("\nFaca sua aposta: ");
        for(j = 0; j < 9; j++){
            scanf("%d",&a[i][j]);
        }
    }
}

void sortea(int n[9]){
    int i;
    printf("\nNumero sorteados: ");
    for(i = 0; i < 9; i++){
        scanf("%d",&n[i]);
    }
}

int compara(int a[5][9], int n[9]){
    int i, j, z, k, s = 0;
    for(i = 0; i < 5; i++){ //repeticao das linhas da matriz
        for(z = 0; z < 9; z++){ //repeticao do array
            k = n[i];
            for(j = 0; j < 9; j++){ //repeticao das colunas da matriz
                if(k == a[i][j]){ //ve se todos os numeros da aposta e sorteados sao iguais
                    s++;
                }
            }
            if(s == 9){ //se todos forem iguais sai da funcao
                break;
            } else {
                s = 0; //se nao forem iguais, a soma volta a 0 e verifica a proxima aposta
            }
        }
    }
    return s;
}

int main(){
    int a[5][9], n[9], i, s = 0;
    preenche(a);
    sortea(n);
    s = compara(a, n);
    if(s == 9){
        printf("Parabens, vc ganhou");
    } else {
        printf("Nao foi dessa vez");
    }

    return 0;
}
